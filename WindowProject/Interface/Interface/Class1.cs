﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1;

namespace Interface
{
    public class DisketPrinter
    {
        public void Print(Disket Disketka)
        {
            Console.WriteLine("Название "+Disketka.Name);
            Console.WriteLine("Длительность " + Disketka.Duration);
        }
    }
}
