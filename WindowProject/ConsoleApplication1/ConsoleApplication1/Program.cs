﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1;
using Interface;

namespace ConsoleApplication1
{
    class Program
    {

        static void Main()
        {



            Disket Disketka = new Disket();
            Disketka = FactoryCreate.CreateDisket();
            
            Disketka.Duration = Disketka.GetDuration();

            DisketPrinter Printer = new DisketPrinter();
            Printer.Print(Disketka);
            Console.ReadLine();



        }

        
    }
}
