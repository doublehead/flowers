﻿using System;

using System.Windows.Forms;

using ClassLibrary1;

namespace MusicWindow

{

    public partial class Form3 : Form

    {

        public byte typeClass;

        public Form3()

        {

            InitializeComponent();

        }
      

        public Form3(MusicComposition music)

        {

            InitializeComponent();

            Type type = music.GetType();

            if (type.Equals(typeof(HardRock)))

            {

                IntHardRock(music);

                typeClass = 0;

            }

            else if (type.Equals(typeof(Rock)))

            {

                IntRock(music);

                typeClass = 1;

            }

            else if (type.Equals(typeof(Jazz)))

            {

                IntJazz(music);

                typeClass = 2;

            }

        }

        private void buttonClose_Click(object sender, EventArgs e)

        {

            Close();

        }

        private void buttonOK_Click(object sender, EventArgs e)

        {
            if (typeClass == 0) EditHardRock();

            else if (typeClass == 1) EditRock();

            else if (typeClass == 2) EditJazz();

        }

        private void IntHardRock(MusicComposition music)

        {

            labelType.Text = "HardRock";

            textBoxUn1.Visible = true;

            textBoxUn2.Visible = true;

           

            labelUn1.Visible = true;

            labelUn2.Visible = true;

           

            labelUn1.Text = "NumberofGuitarists";
            labelUn2.Text = "Followers";

            HardRock tr = (HardRock)music;

            textBoxDuration.Text = Convert.ToString(tr.Duration);

            textBoxName.Text = tr.Name;

            textBoxQuantity.Text = Convert.ToString(tr.Quantity);

            textBoxUn1.Text = Convert.ToString(tr.NumberOfGuitarists);
            textBoxUn2.Text = Convert.ToString(tr.Followers);

        }

        private void IntRock(MusicComposition music)

        {

            labelType.Text = "Rock";

            textBoxUn1.Visible = true;

            textBoxUn2.Visible = false;

            

            labelUn1.Visible = true;

            labelUn2.Visible = false;

        

            labelUn1.Text = "NumberOfGuitarists";

           

            Rock tr = (Rock)music;

            textBoxDuration.Text = Convert.ToString(tr.Duration);

            textBoxName.Text = tr.Name;

            textBoxQuantity.Text = Convert.ToString(tr.Quantity);

            textBoxUn1.Text = Convert.ToString(tr.NumberOfGuitarists);

            

        }

        private void IntJazz(MusicComposition music)
        { 
    
labelType.Text = "Jazz";

textBoxUn1.Visible = true;

textBoxUn2.Visible = false;



labelUn1.Visible = true;

labelUn2.Visible = false;


labelUn1.Text = "Melody";





           Jazz tr = (Jazz)music;

            textBoxDuration.Text = Convert.ToString(tr.Duration);

            textBoxName.Text = tr.Name;

            textBoxQuantity.Text = Convert.ToString(tr.Quantity);

            textBoxUn1.Text = Convert.ToString(tr.Melody);

        }

    private void EditHardRock()

    {

        HardRock newHardRock = new HardRock();

            newHardRock.Name = textBoxName.Text;

            newHardRock.Duration = Math.Abs(Convert.ToDouble(textBoxDuration.Text));

            newHardRock.Quantity = Convert.ToInt16(textBoxQuantity.Text);

            newHardRock.Followers = Convert.ToInt16(textBoxUn2.Text);
            newHardRock.NumberOfGuitarists= Convert.ToInt16(textBoxUn1.Text);

            Close();

        Form1.SelfRef.EditDisket(newHardRock);

    }

    private void EditRock()

    {

            Rock newRock = new Rock();

            newRock.Name = textBoxName.Text;

            newRock.Duration = Math.Abs(Convert.ToDouble(textBoxDuration.Text));

            newRock.Quantity = Convert.ToInt16(textBoxQuantity.Text);

            
            newRock.NumberOfGuitarists = Convert.ToInt16(textBoxUn1.Text);

            Close();

            Form1.SelfRef.EditDisket(newRock);

        }

    private void EditJazz()

    {

            Jazz newJazz = new Jazz();

            newJazz.Name = textBoxName.Text;

            newJazz.Duration = Math.Abs(Convert.ToDouble(textBoxDuration.Text));

            newJazz.Quantity = Convert.ToInt16(textBoxQuantity.Text);


            newJazz.Melody = Convert.ToInt16(textBoxUn1.Text);

            Close();

            Form1.SelfRef.EditDisket(newJazz);

        }

}

}
