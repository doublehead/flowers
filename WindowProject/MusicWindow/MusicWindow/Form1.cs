﻿using System;

using System.Linq;

using ClassLibrary1;

using Interface;

using System.Windows.Forms;


namespace MusicWindow

{

    public partial class Form1 : Form

    {

        public static Form1 SelfRef { get; set; }

        public Disket Disketka;

        public Form1()

        {

            SelfRef = this;

            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)

        {

            Disketka = FactoryCreate.CreateDisket();

            ReCalc();

            foreach (var item in Disketka.MyDisket)
            {
                listBoxDisket.Items.Add(item.Name);
            }

        }

        private void ButtonAdd_Click(object sender, EventArgs e)

        {

            Form2 newFormAdd = new Form2();

            newFormAdd.ShowDialog();

        }

        private void ButtonEdit_Click(object sender, EventArgs e)

        {

            if (listBoxDisket.SelectedIndex != -1)

            {

                int ind = listBoxDisket.SelectedIndex;

                Form3 newFormEdit = new Form3(Disketka.MyDisket[ind]);

                newFormEdit.ShowDialog();

            }

        }

        private void ButtonDelete_Click(object sender, EventArgs e)

        {

            if (listBoxDisket.SelectedIndex != -1)

            {

                int i = listBoxDisket.SelectedIndex;

                listBoxDisket.Items.RemoveAt(i);

               Disketka.MyDisket.RemoveAt(i);

            }

            ReCalc();

        }

        public void EditDisket(MusicComposition music)

        {

            int ind = listBoxDisket.SelectedIndex;

            Disketka.MyDisket[ind] = music;

            listBoxDisket.Items.RemoveAt(ind);

            listBoxDisket.Items.Insert(ind, music.Name);

            ReCalc();

        }

        private void ReCalc()

        {

            //Подсчет стоимости автопарка

            LabelDuration.Text = "Duration="+Convert.ToString(Disketka.GetDuration());

        }

        public void AddMusicInDisket(MusicComposition music)

        {

            listBoxDisket.Items.Add(music.Name);

            Disketka.MyDisket.Add(music);

            ReCalc();

        }

        //private void InitializeComponent()
        //{
        //    this.SuspendLayout();
        //    // 
        //    // Form1
        //    // 
        //    this.ClientSize = new System.Drawing.Size(284, 262);
        //    this.Name = "Form1";
        //    this.ResumeLayout(false);

        //}
    }

}
