﻿using System;

using System.Windows.Forms;

using ClassLibrary1;

  

namespace MusicWindow

{

    public partial class Form2 : Form

    {

        public Form2()

        {

            InitializeComponent();

        }

        private void Form2_Load(object sender, EventArgs e)

        {

            ComboBoxCreate();

        }

        private void comboBox1_DropDownClosed(object sender, EventArgs e)

        {

            int ind = comboBox1.SelectedIndex;

            if (ind == 0) AddHardRock();

            else if (ind == 1) AddRock();

            else if (ind == 2) AddJazz();

        }

        private void buttonOK_Click(object sender, EventArgs e)

        {

            int ind = comboBox1.SelectedIndex;

            if (ind == 0) CreateHardRock();

            else if (ind == 1) CreateRock();

            else if (ind == 2) CreateJazz();

        }

        private void buttonClose_Click(object sender, EventArgs e)

        {

            Close();

        }

        private void ComboBoxCreate()

        {

            comboBox1.Items.Add("HardRock");

            comboBox1.Items.Add("Rock");

            comboBox1.Items.Add("Jazz");

            comboBox1.SelectedIndex = 0;

            AddHardRock();

        }

        private void AddHardRock()

        {

            textBoxUn1.Visible = true;

            textBoxUn2.Visible = true;

            

            labelUn1.Visible = true;

            labelUn2.Visible = true;

           

            labelUn1.Text = "NumberOfGuitarists";
            labelUn2.Text = "Followers";


        }

        private void AddRock()

        {

            textBoxUn1.Visible = true;

            textBoxUn2.Visible = false;

            

            labelUn1.Visible = true;

            labelUn2.Visible = false;



            labelUn1.Text = "AgeOfGuitarists";

          

        }

        private void AddJazz()

        {

            textBoxUn1.Visible = true;

            textBoxUn2.Visible = false;



            labelUn1.Visible = true;

            labelUn2.Visible = false;



            labelUn1.Text = "Melody";

        }

        private void CreateHardRock()

        {

            HardRock newHardRock = new HardRock();

            newHardRock.Name = textBoxName.Text;

            newHardRock.Duration = Math.Abs(Convert.ToDouble(textBoxDuration.Text));

            newHardRock.Quantity = Convert.ToInt16(textBoxQuantity.Text);

            newHardRock.NumberOfGuitarists = Convert.ToInt16(textBoxUn1.Text);

            newHardRock.Followers = Convert.ToInt16(textBoxUn2.Text);
            Close();

            Form1.SelfRef.AddMusicInDisket(newHardRock);

        }

        private void CreateRock()

        {

            Rock newRock = new Rock();

            newRock.Name = textBoxName.Text;

            newRock.Duration = Math.Abs(Convert.ToDouble(textBoxDuration.Text));

            newRock.Quantity = Convert.ToByte(textBoxQuantity.Text);

            newRock.NumberOfGuitarists = Convert.ToInt16(textBoxUn1.Text);

            

            Close();

            Form1.SelfRef.AddMusicInDisket(newRock);

        }

        private void CreateJazz()

        {

            Jazz newJazz = new Jazz();

            newJazz.Name = textBoxName.Text;

            newJazz.Duration = Math.Abs(Convert.ToDouble(textBoxDuration.Text));

            newJazz.Quantity = Convert.ToInt16(textBoxQuantity.Text);

            newJazz.Melody = Convert.ToInt16(textBoxUn1.Text);

            

            Close();

            Form1.SelfRef.AddMusicInDisket(newJazz);

        }

    }

}