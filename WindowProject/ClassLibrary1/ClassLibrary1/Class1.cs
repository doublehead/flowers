﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace ClassLibrary1
{
    public class Disket:MusicComposition
    {
        public double DisketDuration { get; set; }
        public List<MusicComposition> MyDisket { get; set; }
        public double GetDuration()
        {
            DisketDuration = 0;
            foreach (var item in MyDisket)
            {
                DisketDuration += item.Duration * item.Quantity;
            }
            return DisketDuration;
        }
        //public void AddComposition(MusicComposition Composition)
        //{
        //    List<MusicComposition> MyDisk = new List<MusicComposition>();
        //    MyDisk.Add(Composition);
        //}
        

    }
    public class FactoryCreate
    {
        public static Disket CreateDisket()
        {
            Disket Disketka = new Disket();
            Disketka.Name = "Popuri";
            //создание объектов
            Rock rock = new Rock();
            rock.Name = "Bad Habbit";
            rock.Duration = 201;
            rock.NumberOfGuitarists = 8;
            rock.Quantity = 1;


            Jazz jazz = new Jazz();
            jazz.Name = "Blues";
            jazz.Duration = 215;
            jazz.Melody = 25;
            jazz.Quantity = 1;

            //добавление оружия в "подразделение"
            List<MusicComposition> MyDisket = new List<MusicComposition>();
            MyDisket.Add(rock);
            MyDisket.Add(jazz);
            Disketka.MyDisket = MyDisket;
            return Disketka;



        }
    }

    public class MusicComposition
        {
            private string _name;
            private double _duration;

            public int Quantity { get; set; }


            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }


            public double Duration
            {
                get { return _duration; }
                set { _duration = value; }
            }




        }

        public class Rock : MusicComposition
        {
            private int _numberofguitarists;

            public int NumberOfGuitarists
            {
                get { return _numberofguitarists; }
                set { _numberofguitarists = value; }
            }
        }
        public class Jazz : MusicComposition
        {
            private int _melody;

            public int Melody
            {
                get { return _melody; }
                set { _melody = value; }
            }
        }
        public class HardRock : Rock
        {
            private int _followers;

            public int Followers
            {
                get { return _followers; }
                set { _followers = value; }
            }
        }

    }

